pragma solidity ^0.5.6;

import './OptionMaster.sol';

contract PutOptionMaster is OptionMaster {

	uint public spotPrice;

	// Seller functions

	constructor(uint _spotPrice, uint _strikePrice, uint _premium, uint _expiry, bool _optionSold, address payable _oracle)
		OptionMaster(OptionType.PUT, msg.value, _strikePrice, _premium, _expiry, _optionSold, _oracle) public payable {
		spotPrice = _spotPrice;
		notional = (msg.value - oracleFee) * (spotPrice / strikePrice);
	}
	
	function getValues() public view returns(OptionType, uint, uint, uint, uint, uint, address, bool, bool, bool, uint) {
		return (optionType, notional, spotPrice, strikePrice, premium, expiry, oracle, bought, sold, priceFed, finalPrice);
	}
	
	// Buyer functions
	
	function buy() public payable option_not_bought before_expiry  { 
		require (msg.value == notional + premium);
		
		buyer = msg.sender;
		bought = true;
		buyer.transfer(strikePrice * notional / spotPrice);
		seller.transfer(premium);
	}

// Oracle functions

	function feedPrice(uint _finalPrice) public only_oracle option_bought after_expiry price_not_fed {

		finalPrice = _finalPrice;
		priceFed = true;
		
		oracle.transfer(oracleFee);
		
		if (finalPrice > strikePrice) {
			seller.transfer(notional * strikePrice / finalPrice);
      buyer.transfer(notional - (notional * strikePrice) / finalPrice);
		}
		else {
			seller.transfer(notional);
		}

	}
}
