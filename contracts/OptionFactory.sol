pragma solidity ^0.5.6;

import './OptionMaster.sol';
import './CallOptionMaster.sol';
import './PutOptionMaster.sol';

contract OptionFactory {

	struct Option {
		address optionAddress;
		OptionMaster.OptionType optionType;
	}

	Option[] public options;

	event OptionCreated(address optionAddress, OptionMaster.OptionType optionType);
	
	function numDeployedOptions() public view returns(uint number) {
		return options.length;
	}
	
	function createCallOption(uint _strikePrice, uint _premium, uint _expiry, address payable _oracle) public payable returns(address contractAddress) {
		CallOptionMaster option = (new CallOptionMaster).value(msg.value)(_strikePrice, _premium, _expiry, true, _oracle);
		
		options.push(Option(address(option), OptionMaster.OptionType.CALL));
		emit OptionCreated(address(option), OptionMaster.OptionType.CALL);
		return address(option);
	}
	
	function createPutOption(uint _spotPrice, uint _strikePrice, uint _premium, uint _expiry, address payable _oracle) public payable returns(address contractAddress) {
		PutOptionMaster option = (new PutOptionMaster).value(msg.value)(_spotPrice, _strikePrice, _premium, _expiry, true, _oracle);
		
		options.push(Option(address(option), OptionMaster.OptionType.PUT));
		emit OptionCreated(address(option), OptionMaster.OptionType.PUT);
		return address(option);
	}

}
