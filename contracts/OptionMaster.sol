pragma solidity ^0.5.6;

contract OptionMaster {

	enum OptionType {CALL,PUT}

	OptionType public optionType;

	address payable public buyer; // @TODO: Remove public before deployment
	address payable public seller;

	uint public notional;
	uint public strikePrice;
	uint public premium;
	uint public expiry;
	address payable public oracle;
	uint public oracleFee;

	uint public finalPrice;

	bool public bought;
	bool public sold;
	bool public priceFed;

// Modifiers

	// Party

	modifier only_oracle {
		require (msg.sender == oracle);
		_;
	}

	// Contract state

	modifier option_not_sold {
		require (sold == false);
		_;
	}

	modifier option_sold {
		require (sold == true);
		_;
	}

	modifier option_not_bought {
		require (bought == false);
		_;
	}

	modifier option_bought {
		require (bought == true);
		_;
	}

	modifier price_not_fed {
		require (priceFed == false);
		_;
	}

	// Timing

	modifier before_expiry {
		require (true); //(now < expiry);
		_;
	}

	modifier after_expiry {
		require (true); //(now < expiry);
		_;
	}


	// Seller functions

	constructor(OptionType _type, uint _notional, uint _strikePrice, uint _premium, uint _expiry, bool _optionSold, address payable _oracle) public {
	
		oracleFee = 100000000000000000 wei;
		seller = tx.origin;
		optionType = _type;
		notional = _notional - oracleFee;
		strikePrice = _strikePrice;
		premium = _premium;
		expiry = _expiry;
		sold = _optionSold;
		oracle = _oracle;
		bought = false;
		priceFed = false;
			
	}

	// Buyer functions

  function buy() public payable;

	// Oracle functions

  function feedPrice(uint _finalPrice) public;

}
