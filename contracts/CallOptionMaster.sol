pragma solidity ^0.5.6;

import './OptionMaster.sol';

contract CallOptionMaster is OptionMaster {


	// Seller functions

	constructor(uint _strikePrice, uint _premium, uint _expiry, bool _optionSold, address payable _oracle)
		OptionMaster(OptionType.CALL, msg.value, _strikePrice, _premium, _expiry, _optionSold, _oracle) public payable {
	}

	function getValues() public view returns(OptionType, uint, uint, uint, uint, address, bool, bool, bool, uint) {
		return (optionType, notional, strikePrice, premium, expiry, oracle, bought, sold, priceFed, finalPrice);
	}

	// Buyer functions

	function buy() public payable option_sold option_not_bought before_expiry {
		require (premium == msg.value);
		seller.transfer(msg.value);
		buyer = msg.sender;
		bought = true;
	}

	// Oracle functions

	function feedPrice(uint _finalPrice) public only_oracle option_bought after_expiry price_not_fed {

		finalPrice = _finalPrice;
		priceFed = true;

		oracle.transfer(oracleFee);

		if (finalPrice >= strikePrice) {
			seller.transfer(notional - (notional * (strikePrice / finalPrice)));
			buyer.transfer(notional * (strikePrice / finalPrice));
		}
		else {
			seller.transfer(notional);
		}

	}
    
}
