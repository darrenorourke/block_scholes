
import contracts from './contracts.json'

export const OptionTypes = {
	CALL: 0,
	PUT: 1
}

const getOption = (address, type) => {
	const option = window.web3.eth.contract(
		type === OptionTypes.CALL ? contracts.callmaster.abi : contracts.putmaster.abi
	).at(address)
	return new Promise((resolve, reject) => {
		option.getValues((err, values) => {
			if (err) {
				console.error(err)
				reject(err)
			}
			var idx = 0
			resolve({
				type: values[idx++].c[0],
				notional: Number(window.web3.fromWei(values[idx++].toString())),
				spot: type === OptionTypes.PUT ? Number(values[idx++].toString()) : undefined,
				strike: Number(values[idx++].toString()),
				premium: Number(window.web3.fromWei(values[idx++].toString())),
				expiry: new Date(values[idx++].c[0]).toLocaleDateString(),
				oracle: values[idx++],
				bought: values[idx++],
				sold: values[idx++],
				priceFed: values[idx++],
				finalPrice: Number(values[idx].toString()),
				address: address
			})
		})
	})
}

export default getOption