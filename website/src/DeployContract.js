import React from 'react'
import { OptionTypes } from './option'

export default class DeployContract extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      type: OptionTypes.CALL,
      notional: React.createRef(),
      premium: React.createRef(),
      spot: React.createRef(),
      strike: React.createRef(),
      expiry: React.createRef(),
      oracle: React.createRef()
    }
  }

  handleTypeChange = (e) => {
    this.setState({
      type: Number(e.target.value)
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let oracleFee = 0.1
    let notional = Number(this.state.notional.current.value)
    let premium = window.web3.toWei(this.state.premium.current.value, 'ether')
    let spot = this.state.type === 'Put' && this.state.spot.current.value
    let strike = this.state.strike.current.value
    let oracle = this.state.oracle.current.value
    let expiry = (new Date(this.state.expiry.current.value)).getTime()
    let contract = this.props.contract
    console.dir(this.props.contract)
    if (this.state.type === OptionTypes.CALL) {
      contract.createCallOption(strike, premium, expiry, oracle, { value: window.web3.toWei(notional + oracleFee, 'ether') }, (err, hash) => {
        err && console.error(err)
        console.log(hash)
      })
    } else {
      contract.createPutOption(spot, strike, premium, expiry, oracle, { value: window.web3.toWei(notional * (strike / spot) + oracleFee, 'ether') }, (err, hash) => {
        err && console.error(err)
        console.log(hash)
      })
    }
  }

  render() {
    return (
      <div>
        <h4>Offer Option</h4>
        <form onSubmit={this.handleSubmit}>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label className="input-group-text" htmlFor="type">Type</label>
            </div>
            <select className="custom-select" id="type" onChange={this.handleTypeChange}>
              <option value={OptionTypes.CALL}>Call</option>
              <option value={OptionTypes.PUT}>Put</option>
            </select>
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Notional</span>
            </div>
            <input
              className="form-control"
              name="notional"
              ref={this.state.notional}
              autoComplete="off"
              required
            />
          </div>
          {
            this.state.type === OptionTypes.PUT ?
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">Spot Price</span>
                </div>
                <input
                  className="form-control"
                  name="spot"
                  ref={this.state.spot}
                  autoComplete="off"
                  required
                />
              </div>
            :
              undefined
          }
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Strike Price</span>
            </div>
            <input
              className="form-control"
              name="strike"
              ref={this.state.strike}
              autoComplete="off"
              required
            />
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Premium</span>
            </div>
            <input
              className="form-control"
              name="premium"
              ref={this.state.premium}
              autoComplete="off"
              required
            />
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Expiry Date</span>
            </div>
            <input
              className="form-control"
              type='date'
              name="expiry"
              ref={this.state.expiry}
              autoComplete="off"
              required
            />
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text">Oracle Address</span>
            </div>
            <input
              className="form-control"
              name="oeracle"
              ref={this.state.oracle}
              autoComplete="off"
              required
            />
          </div>
          <div className="input-group col-12 text-right pr-0 mb-3" style={{display: 'block'}}>
            <button type = 'submit' className="btn btn-primary" >Create Option</button>
          </div>
        </form>
      </div>
    ) 
  }  
}
