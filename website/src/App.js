import React, { Component } from 'react'
import DeployContract from './DeployContract'
import OptionList from './OptionList'
import getOption from './option'
import contracts from './contracts.json'
import './App.css'
//import '@mdi/font/css/materialdesignicons.css'

class App extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      factory: React.createRef(),
      remember: false,
      connectedToNetwork: false,
      instance: null,
      options: []
    }
  }
  
  componentDidMount() {
    let instance
    const settings = JSON.parse(localStorage.getItem('blockscholes'))
    if (settings && settings.defaultFactoryAddress) {
      this.state.factory.current.value = settings.defaultFactoryAddress
      instance = window.web3.eth.contract(contracts.factory.abi).at(settings.defaultFactoryAddress)
      instance.OptionCreated({}, async (err, res) => {
        if (err) {
          console.error(err)
          return
        }
        this.setState({
          options: [...this.state.options, await getOption(res.args.optionAddress, res.args.optionType.c[0])]
        })
      })
      /*// @warning Web3 1.0 is in Beta
      , (error, event) => {
          console.log(event)
      }).on('data', (event) => {
        console.log(event) // same results as the optional callback above
      })
      .on('changed', (event) => {
        // remove event from local database
      })
      .on('error', console.error)
      */
      this.setState({
        remember: true,
        instance
      })
      this.updateOptions(instance)
    }

    window.addEventListener('load', async () => {
      
      if (window.ethereum) {
        try {
          // Request account access if needed
          const accounts = await window.ethereum.enable()
          window.web3.eth.defaultAccount = accounts[0]
          window.ethereum.on('accountsChanged', accounts => {
            window.web3.eth.defaultAccount = accounts[0]
            this.forceUpdate()
          })
        } catch (error) {
          // User denied account access...
          console.error(error)
          return
        }
      }
      // Legacy dapp browsers...
      else if (window.web3) {
        window.web3 = new window.Web3(window.web3.currentProvider)
      }
      // Non-dapp browsers...
      else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
        return
      }

      this.setState({
        connectedToNetwork: true
      })

    })

  }

  setFactory = (e) => {
    const { factory, remember } = this.state
    const instance = window.web3.eth.contract(contracts.factory.abi).at(factory.current.value)
    e.preventDefault()
    
    if (remember) {
      localStorage.setItem('blockscholes', JSON.stringify({ defaultFactoryAddress: factory.current.value }))
    }

    instance.OptionCreated({}, async (err, res) => {
      if (err) {
        console.error(err)
        return
      }
      this.setState({
        options: [...this.state.options, await getOption(res.args.optionAddress, res.args.optionType.c[0])]
      })
    })
    /*// @warning Web3 1.0 is in Beta
    , (error, event) => {
        console.log(event)
    }).on('data', (event) => {
      console.log(event) // same results as the optional callback above
    })
    .on('changed', (event) => {
      // remove event from local database
    })
    .on('error', console.error)
    */
    
    this.setState({
      instance
    })
    this.updateOptions(instance)
    
  }

  updateOptions = (instance) => {

    instance.numDeployedOptions((err, res) => {
      if (err) {
        console.error(err)
        return
      }
      const options = []
      for (let i = 0; i < res.c[0]; i++) {
        instance.options(i, async (err, opt) => {
          if (err) {
            console.error(err)
            return
          }
          options.push(await getOption(opt[0], opt[1].c[0]))
          this.setState({
            options
          })
        })
      }
    })
  }

  updateOption = async (idx) => {
    const options = Array.from(this.state.options)
    options[idx] = await getOption(options[idx].address, options[idx].type)
    this.setState({
      options
    })
  }
  
  handleRememberAddressChange = (e) => {
    let settings = localStorage.getItem('blockscholes')
    settings = settings ? JSON.parse(settings) : {}
    if (settings.defaultFactoryAddress) delete settings.defaultFactoryAddress
    if (this.state.remember) {
      localStorage.setItem('blockscholes', JSON.stringify(settings))
    } else {
      localStorage.setItem('blockscholes', JSON.stringify({...settings, defaultFactoryAddress: this.state.factory.current.value}))
    }
    this.setState({
      remember: !this.state.remember
    })
  }

  render() {
    const { connectedToNetwork } = this.state
    return (
      <div>
        <div className="container" style={{marginBottom: '3rem'}}>
          <div className="row mt-4 align-items-end">
            <div className="col-6">
              <h4>Option Creator</h4>
            </div>
            <div className="col-6 text-right">
              <img src="/images/logo_transparent.png" style={{width: 100}}/>
            </div>
            <div className="col-sm mt-2">
              <form name="setContractAddresses" onSubmit={this.setFactory}>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">Factory Contract</span>
                  </div>
                  <input
                    className="form-control"
                    name="factory"
                    ref={this.state.factory}
                    autoComplete="off"
                    required
                  />
                  <div className="input-group-append">
                    <div className="input-group-text">
                      <input
                        type="checkbox"
                        aria-describedby="remember"
                        aria-label="Remember this address?"
                        checked={this.state.remember}
                        onChange={this.handleRememberAddressChange}
                      />
                    </div>
                  </div>
                </div>
                <small id="remember" className="form-text text-muted text-right" style={{marginTop: '-1rem', marginBottom: '1rem'}}>Remember this address?</small>
                <button type='submit' className="btn btn-primary float-right" >Set Factory</button>
              </form>
            </div>
          </div>
          {
            this.state.instance && <DeployContract contract={this.state.instance} abi={contracts} />
          }
          {
            this.state.instance && <OptionList options={this.state.options} onOptionUpdated={this.updateOption}/>
          }
        </div>
        <nav className="navbar fixed-bottom justify-content-end navbar-light bg-light">
          <div className="container">
            <div className="col-7">Account: {window.web3 && window.web3.eth.defaultAccount ? `${window.web3.eth.defaultAccount.substr(0, 6)}...${window.web3.eth.defaultAccount.substr(38)}` : 'Unkown'}</div>
            <div className={`col-5 text-right ${connectedToNetwork ? 'text-success' : 'text-danger'}`}>[Network {connectedToNetwork ? 'Connected' : 'Disconnected'}] <i className="mdi mdi-wifi" aria-hidden="true" style={{ marginLeft: "0.5em" }}></i></div>
          </div>
        </nav>
      </div>
    )
  }
}

export default App
