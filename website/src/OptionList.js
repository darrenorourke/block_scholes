import React from 'react'
import contracts from './contracts.json'
import { OptionTypes } from './option'

export default class OptionList extends React.Component {

  state = {
    feedPriceRefs: []
  }

  componentWillReceiveProps(nextProps) {
    const { feedPriceRefs } = this.state
    if (feedPriceRefs && nextProps.options.length !== feedPriceRefs.length) {
      const newRefs = nextProps.options.slice(
        nextProps.options.length - (nextProps.options.length - feedPriceRefs.length)
      ).map(() => React.createRef())
      this.setState({
        feedPriceRefs: [...feedPriceRefs, ...newRefs]
      })
    }
  }

  handleBuy = (evt, option, idx) => {
    evt.preventDefault()
    const instance = option.type === OptionTypes.CALL ?
      window.web3.eth.contract(contracts.callmaster.abi).at(option.address) :
      window.web3.eth.contract(contracts.putmaster.abi).at(option.address)
    const value = window.web3.toWei(
      option.type === OptionTypes.CALL ? option.premium : option.notional + option.premium,
      'ether'
    )
    instance.buy({}, { value }, (err, res) => {
      if (err) {
        console.error(err)
      }
      else {
        console.log(res)
        this.props.onOptionUpdated(idx)
      }
    })
  }

  handleFeedPrice = (evt, option, idx) => {
    evt.preventDefault()
    const instance = option.type === OptionTypes.CALL ?
      window.web3.eth.contract(contracts.callmaster.abi).at(option.address) :
      window.web3.eth.contract(contracts.putmaster.abi).at(option.address)
    instance.feedPrice(Number(this.state.feedPriceRefs[idx].current.value), {}, (err, res) => {
      if (err) {
        console.error(err)
      }
      else {
        console.log(res)
        this.props.onOptionUpdated(idx)
      }
    })
  }

  render() {
    const { options } = this.props
    return (
      <div>
        <h4>Deployed Options</h4>
        <form>
          <ul className="list-group col-12 pr-0">
            {
              options.map((option, idx) => {
                return (
                  <li key={idx} className="list-group-item list-group-item-action flex-column align-items-start">
                    <div className="d-flex w-100 justify-content-between">
                      <h5 className="mb-1">Address: {option.address}</h5>
                      <span className="flaot-right badge badge-pill badge-secondary" style={{paddingTop: '0.5rem'}}>
                        {option.type === OptionTypes.CALL ? 'Call' : 'Put'}
                      </span>
                    </div>
                    <p>
                      {`Notional: ${option.notional} | Premium: ${option.premium} ${option.type === OptionTypes.PUT ? `| Spot: ${option.spot} ` : '' }| Strike: ${option.strike} | Expiry: ${option.expiry} | Bought: ${option.bought} | Price Fed: ${option.priceFed} | Final Price: ${option.finalPrice}`}
                    </p>
                    <p>
                      {`Oracle: ${option.oracle}`}
                    </p>
                    {
                      !option.bought &&
                      <div className="input-group mb-3 justify-content-end">
                        <button className={`btn btn-primary float-right ${!option.address ? 'disabled' : ''}`} onClick={e => this.handleBuy(e, option, idx)}>Buy</button>
                      </div>
                    }
                    {
                      option.bought && !option.priceFed &&
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Feed Price</span>
                        </div>
                        <input
                          className="form-control"
                          name="feedPrice"
                          ref={this.state.feedPriceRefs[idx]}
                          autoComplete="off"
                          required
                        />
                        <div className="input-group-append">
                          <button className={`btn btn-primary float-right ${!option.bought || !option.address ? 'disabled' : ''}`} onClick={e => this.handleFeedPrice(e, option, idx)}>Feed</button>
                        </div>
                      </div>
                    }
                  </li>
                )
              })
            }
          </ul>
        </form>
      </div>
    ) 
  }  
}
