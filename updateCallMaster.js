var callOptMaster = require("./build/contracts/CallOptionMaster.json");
var optionFactory = require("./build/contracts/OptionFactory.json");
var putOptMaster = require("./build/contracts/PutOptionMaster.json")

var contracts = require("./website/src/contracts.json");

contracts.callmaster = callOptMaster
contracts.factory = optionFactory
contracts.putmaster = putOptMaster

var fs = require("fs");
var json = JSON.stringify(contracts, null, 4);
fs.writeFile("./website/src/contracts.json", json, (err) => {
  if (err) {
      console.error(err);
      return;
  };
  console.log("File has been created");
});
